odklocal is a set of R scripts for parsing Open Data Kit data from a phone/tablet, without internet, into reviewable data in the R programming language.
It is similar to ODK Briefcase in purpose but works on variants of ODK data like KoboCollect.

## How to use

1. Copy your ODK data from your mobile device via usb, or memory card to a computer.
2. main.R has code to parse a specified form into a dataframe and write the results as a csv.
3. collectPhotos.R moves all your photos into one directory from the nested directory structure on the mobile device.