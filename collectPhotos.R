# Alex Mandel 
# Dec 8,2017
# University of California, Davis
# Collects photos from odk subfolders, filters by form, and copies to a new top level directory.

# Set the form name you want to collect photos for.
formname <- "KateAg"

# Directory where your instances folder is located
dr <- "odk/instances"

#find images
images <- list.files(dr, recursive = TRUE, pattern = "jpg$", full.names = TRUE)

#filter by form
images <- images[grep(formname,images)]

# set output
photodir <- paste0(formname,"-Photos")
dir.create(photodir)

# copy images
file.copy(images,photodir)
